<?php

use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Models\Classification;
use function Pest\Laravel\get;
use function Pest\Laravel\post;
use function Pest\Laravel\signIn;

it('test the upload of an image in the database', function () {
    // Create a user
    $user = User::factory()->create();

    // Authenticate the user
    $this->actingAs($user);

    // Create a fake uploaded file
    Storage::fake('public');
    $file = UploadedFile::fake()->image('test-image.jpg');

    // Send a post request with the upploaded file
    $response = $this->post('image-recognition', ['image' => $file]);

    // Check that the answere is a success
    $response->assertStatus(200);

    // Vérifier que le fichier a été correctement stocké
    $this->assertDatabaseHas('images_classified', [
        'user_id' => $user->id,
    ]);
});

it('test the upload of an image on the server', function () {

    // Get the last enry of the image table and see if the image has been stored locally
    $lastEntry = App\Models\Classification::latest()->first();

    if ($lastEntry) {
        $imagePath = $lastEntry->image;
    }

    // Check if the file exists locally
    $path = public_path($imagePath);
    $this->assertFileExists($path);

});
